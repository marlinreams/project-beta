from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json
from .models import AutomobileVO, Service, Technician

# Create your views here.
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "color",
        "year",
        "vin",
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties =[
        "name",
        "number",
        "id",

    ]

class ServiceEncoder(ModelEncoder):
    model = Service
    properties = [
        "vin",
        "customer_name",
        "DateTime",
        "technician",
        "reason",
        "status",
        "vip",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder= TechnicianEncoder,
            safe = False
            )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder= TechnicianEncoder,
            safe = False
        )

@require_http_methods(["DELETE"])
def api_delete_technicians(request, pk):
    if request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({
            "deleted": count > 0
        })


@require_http_methods(["GET", "POST"])
def api_list_services(request):
    if request.method == "GET":
        services = Service.objects.all()
        return JsonResponse(
            {'services': services},
            encoder = ServiceEncoder,
            safe= False

        )
    else:
        content = json.loads(request.body)
        if "technician" in content:
            try:
                technician = Technician.objects.get(number = content["technician"])
                content["technician"] = technician
            except Technician.DoesNotExist:
                return JsonResponse({
                   {"message": "Not a Technician"}
                })
        service = Service.objects.create(**content)
        return JsonResponse(
            service,
            encoder= ServiceEncoder,
            safe = False
        )

@require_http_methods(["DELETE"])
def api_delete_services(request, pk):
    if request.method == "DELETE":
        count, _ = Service.objects.filter(id=pk).delete()
        return JsonResponse({
            "deleted": count > 0
        })
