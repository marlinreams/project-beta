from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    name  = models.CharField(max_length=200, null=True)
    number = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.name


class Service(models.Model):
    vip = models.BooleanField(default=False)
    vin = models.CharField(max_length=17, unique=True)
    customer_name = models.CharField(max_length=200)
    DateTime = models.DateTimeField(null=True,)
    reason = models.CharField(max_length=1000)
    status = models.CharField(max_length=100,default = False)
    technician = models.ForeignKey(
        Technician,
        related_name="services",
        on_delete=models.PROTECT
    )
    # automobile = models.ForeignKey(
    #     AutomobileVO,
    #     related_name="services",
    #     on_delete=models.PROTECT
    # )


class Service_history(models.Model):
    service = models.ForeignKey(
        Service,
        related_name = "service_historys",
        on_delete=models.PROTECT
    )
