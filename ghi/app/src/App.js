import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './NewTechnician';
import CreateServiceForm from './CreateService';
import ServiceAppointments from './ServiceAppointments';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="new-technician" element={<TechnicianForm />}/>
          <Route path="new-service" element={<CreateServiceForm />}/>
          <Route path="service-appointments" element={<ServiceAppointments />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
