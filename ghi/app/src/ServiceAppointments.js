import React from 'react';


async function loadServices(){
  const response = await fetch ('http://localhost:8080/api/services/');

  if(response.ok){
      const data = await response.json();
      this?.setState({services: data.services});
      console.log(data)
  }
}
loadServices();


class ServiceAppointments extends React.Component {






  render() {
    return (
    <div>
    <div className="pt-4">
    <form className="d-flex">
            <input className="form-control me-2" type="search" placeholder="Search Vin #" aria-label="Search"/>
            <button className="btn btn-outline-success me-2" type="submit">Search</button>
    </form>
    </div>
    <div>
      <table className="table table-striped">

      <thead>
          <tr>
              <th>VIN</th>
              <th>Customer name</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              

          </tr>
      </thead>
      <tbody>
          {this.services?.map((service, i) => {
              return (
                  <tr key={i}>
                      <td>{service.vin}</td>
                      <td>{service.customer_name}</td>
                      <td>{service.DateTime}</td>
                      <td>{service.technician.name}</td>
                      <td>{service.reason}</td>
                      <td><button class="btn btn-outline-success me-2" type="submit">Search</button></td>
                      <td><button class="btn btn-outline-success me-2" type="submit">Search</button></td>
                  </tr>
              );
          })}
      </tbody>
  </table>
</div>
</div>
    );
  }
}

export default ServiceAppointments;
